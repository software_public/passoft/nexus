#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()


setup(
    author="IRIS PASSCAL",
    author_email='software-support@passcal.nmt.edu',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved ::  GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
    ],
    description="StationXML creation GUI",
    entry_points={
        'console_scripts': [
            'nexus=nexus.nexus:main',
            'ms_sum=nexus.ms_sum:main',
        ],
    },
    install_requires=[
        'obspy>=1.3.0',
        'pyside6',
        'cartopy'
        ],
    setup_requires=[],
    extras_require={
        'dev': [
            'flake8',
            'tox',
            'coverage',
        ]
    },
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='nexus',
    name='nexus',
    packages=find_packages(include=['nexus']),
    test_suite='tests',
    url='https://git.passcal.nmt.edu/software_public/passoft/nexus',
    version='2023.4.6.3',
    zip_safe=False,
)
