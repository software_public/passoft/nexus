#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `nexus` package."""

import unittest
import sys

try:
    import nexus
except ImportError:
     pass

class TestNexus(unittest.TestCase):
    """Tests for `nexus` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_import(self):
        if 'nexus' in sys.modules:
            self.assert_(True, "nexus loaded")
        else:
            self.fail()

