#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Channel Widget model play
"""

from PySide6 import QtCore
from obspy import Inventory


class InventoryModel(QtCore.QAbstractListModel):
    def __init__(self, inventory=None, parent=None):
        super().__init__(parent=parent)

        # For IDE -- can del
        if False: assert isinstance(self._inv, Inventory)

        self._inv = inventory

    def rowCount(self, parent):
        return len(self._inv.networks[0].stations)

    def data(self, index, role):
        row = index.row()
        station = self._inv.networks[0].stations[row]
        if role == QtCore.Qt.DisplayRole:
            return self.sta_str(station)

    def sta_str(self, station):
        return '{} :: {}.{:03d} - {}.{:03d}'.format(
            station.code,
            station.start_date.year,
            station.start_date.julday,
            station.end_date.year,
            station.end_date.julday,
            )
