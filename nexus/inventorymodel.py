#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Inventory Model PyQt5
Lloyd Carothers
"""

from PySide6 import QtCore

class Node(object):
    def __init__(self, name, inv_object, parent=None):
        self._name = name
        self._children = []
        self._parent = parent
        self._inv_obj = inv_object
        self.start = '2000'
        self.end = '2001'

        if parent is not None:
            parent.add_child(self)

    def add_child(self, child):
        self._children.append(child)

    def name(self):
        return self._name

    def child(self, row):
        return self._children[row]

    def child_count(self):
        return len(self._children)

    def parent(self):
        return self._parent

    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)

    def __str__(self, tab_level=0):
        output = '  '*tab_level + self._name + '\n'
        tab_level += 1
        for child in self._children:
            output += child.__str__(tab_level)
        tab_level -= 1
        return output


class NetworkNode(Node):
    def __init__(self, name, inv_object, parent=None):
        Node.__init__(self, name, inv_object, parent)
        self._inv_obj = inv_object
        self.start = '2000'
        self.end = '2001'


class StationNode(Node):
    def __init__(self, name, inv_object, parent):
        Node.__init__(self, name, inv_object, parent)
        self._inv_obj = inv_object


class ChannelNode(Node):
    def __init__(self, name, inv_object, parent):
        Node.__init__(self, name, inv_object, parent)
        self._inv_obj = inv_object


class InventoryModel(QtCore.QAbstractItemModel):
    def __init__(self, root, parent=None):
        super().__init__(parent)
        self._root_node = root

    def rowCount(self, parent):
        if not parent.isValid():
            parent_node = self._root_node
        else:
            parent_node = parent.internalPointer()
        return parent_node.child_count()

    def columnCount(self, parent):
        return 3

    def data(self, index, role):
        if not index.isValid():
            return None
        node = index.internalPointer()
        if role == QtCore.Qt.DisplayRole:
            if index.column() == 0:
                return node.name()
            if index.column() == 1:
                return node.start
            if index.column() == 2:
                return node.end

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if section == 0:
                return 'Code'
            elif section == 1:
                return 'Start'
            elif section == 2:
                return 'End'

    def flags(self, parent):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def index(self, row, column, parent):
        if not parent.isValid():
            parent_node = self._root_node
        else:
            parent_node = parent.internalPointer()
        child = parent_node.child(row)
        if child is not None:
            return self.createIndex(row, column, child)
        else:
            return QtCore.QModelIndex()

    def parent(self, index):
        node = index.internalPointer()
        parent = node.parent()
        if parent == self._root_node:
            return QtCore.QModelIndex()
        return self.createIndex(parent.row(), 0, parent)
