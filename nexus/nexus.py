#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Lloyd Carothers
"""

import os
import sys
import traceback

from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtUiTools import loadUiType
from PySide6.QtWidgets import QMessageBox, QHeaderView
from PySide6.QtCore import Qt
 
from obspy import read_inventory
from obspy.core.inventory import Network, Station, Equipment
from obspy.core.inventory.util import Comment

from .NRLWizard import NRLWizard, DATALOGGER, SENSOR
from . import obspyImproved
from . import hardware
from .ChannelSelectDialog import ChannelSelectDialog
from .StationSelectDialog import StationSelectDialog

# function to display status message, initially just print
status_message = print


ELIPSES = '...'

hardware.load()
DLS = hardware.dataloggers
SENSORS = hardware.sensors

# For Pyside6 `loadUiType()`, no longer takes path, must be in sys.path
sys.path.append(os.path.dirname(__file__))

def load_ui(filename):
    '''
    Helper function
    Load a ui file relative to this source file
    '''
    path = os.path.join(os.path.dirname(__file__), filename)
    try:
            ret = loadUiType(path)
    except Exception as e:
        print(e)
        raise e
    return ret

class InventoryNode:
    def __init__(self, name, inv_object=None, parent=None):
        self._children = []
        self._parent = parent
        self._inv_obj = inv_object

        if self._parent is not None:
            self._parent.add_child(self)

    def add_child(self, child):
        self._children.append(child)
        child._parent = self
        return True

    def remove_child(self, child):
        self._children.remove(child)

    def remove(self):
        self._parent.remove_child(self)

    def child(self, row):
        if row >= 0:
            return self._children[row]

    def child_count(self):
        return len(self._children)

    def children(self):
        return self._children

    def parent(self):
        return self._parent

    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)
        else:
            return 0

    def __str__(self, tab_level=0):
        if self._parent is None:
            output = 'ROOT NODE\n'
        else:
            output = '  '*tab_level + self.code + '\n'
        tab_level += 1
        for child in self._children:
            output += child.__str__(tab_level)
        tab_level -= 1
        return output

    def get_data(self, col):
        return self.FM[col].fget(self)

    def set_data(self, col, value):
        setter = self.FM[col].fset
        if setter:
            return setter(self, value)

    # Field / column mapper
    FM = []
    @classmethod
    def col(cls, field):
        '''
        Returns the field row in model from the field property
        '''
        return cls.FM.index(field)

    # property fields
    def get_code(self):
        try:
            return self._inv_obj.code
        except:
            return ''
    def set_code(self, value):
        self._inv_obj.code = value
        return True
    code  = property(get_code, set_code)
    FM.append(code)

    # Place holder for 2nd col
    FM.append('LOC')

    # Place holder for SR
    FM.append('SR')

    def get_start(self):
        return obspyImproved.utc_to_str(self._inv_obj.start_date)
    def set_start(self, value):
        try:
            utc = obspyImproved.utc_from_str(value)
            self._inv_obj.start_date =  utc
        except:
            raise
        parent = self.parent()
        if parent and parent.parent() and parent._inv_obj.start_date > utc:
            parent.set_start(value)
        return True
    start = property(get_start, set_start)

    FM.append(start)

    def get_end(self):
        return obspyImproved.utc_to_str(self._inv_obj.end_date)
    def set_end(self, value):
        try:
            utc = obspyImproved.utc_from_str(value)
            self._inv_obj.end_date =  utc
        except:
            raise
        parent = self.parent()
        if parent and parent.parent() and parent._inv_obj.end_date < utc:
            parent.set_end(value)
        return True
    end = property(get_end, set_end)
    FM.append(end)

    def has_response(self):
        try:
            if self._inv_obj.response:
                return True
            else:
                return False
        except AttributeError:
            pass
        return all((child.has_response for child in self._children))
    has_response = property(has_response)
    FM.append(has_response)


class NetworkNode(InventoryNode):
    def __init__(self, name, inv_object, parent=None):
        InventoryNode.__init__(self, name, inv_object, parent)
        self._inv_obj = inv_object

    FM = InventoryNode.FM

    def get_description(self):
        return self._inv_obj.description
    def set_description(self, value):
        self._inv_obj.description = value
        return True
    description = property(get_description, set_description)
    FM.append(description)



class StationNode(InventoryNode):
    def __init__(self, name, inv_object, parent):
        InventoryNode.__init__(self, name, inv_object, parent)
        self._inv_obj = inv_object

    FM = InventoryNode.FM

    def get_latitude(self):
        return str(self._inv_obj.latitude)
    def set_latitude(self, value):
        try:
            value = float(value)
        except ValueError:
            return False
        self._inv_obj.latitude = value
        for chan in self._inv_obj.channels:
            chan.latitude = value
    latitude = property(get_latitude, set_latitude)
    FM.append(latitude)

    def get_longitude(self):
        return str(self._inv_obj.longitude)
    def set_longitude(self, value):
        try:
            value = float(value)
        except ValueError:
            return False
        self._inv_obj.longitude = value
        for chan in self._inv_obj.channels:
            chan.longitude = value
    longitude = property(get_longitude, set_longitude)
    FM.append(longitude)

    def get_elevation(self):
        return str(self._inv_obj.elevation)
    def set_elevation(self, value):
        try:
            value = float(value)
        except ValueError:
            return False
        self._inv_obj.elevation = value
        for chan in self._inv_obj.channels:
            chan.elevation = value
        return True
    elevation = property(get_elevation, set_elevation)
    FM.append(elevation)

    def get_depth(self):
        depths = set([str(c.depth) for c in self._inv_obj.channels])
        return '|'.join(depths)
    def set_depth(self, value):
        try:
            value = float(value)
        except ValueError:
            return False
        # self._inv_obj.depth = value
        for chan in self._inv_obj.channels:
            chan.depth = value
        return True
    depth_station = property(get_depth, set_depth)
    FM.append(depth_station)

    def get_dl_sn(self):
        #No channels
        channels = self._inv_obj.channels
        if len(channels) < 1:
            return 'Na'
        try:
            return channels[0].data_logger.serial_number
        except AttributeError:
            return 'Na'
    def set_dl_sn(self, value):
        for chan in self._inv_obj.channels:
            try:
                chan.data_logger.serial_number = value
            except AttributeError:
                chan.data_logger = Equipment(
                    serial_number = value)
        return True
    dl_sn = property(get_dl_sn, set_dl_sn)
    FM.append(dl_sn)


    def get_dl_gain(self):
        #No channels
        channels = self._inv_obj.channels
        try:
            return channels[0].data_logger.gain
        except (AttributeError, IndexError):
            pass
        for chan in (chan for chan in channels
                     if chan.code[1] in ('H', 'L', 'G', 'N')):
            try:
                gain = chan.response.response_stages[1].stage_gain
            except (AttributeError, IndexError):
                continue
            self.set_dl_gain(gain, keep_response=True)
            return gain
        return 'Na'

    def set_dl_gain(self, value, keep_response=False):
        try:
            value = int(value)
        except ValueError:
            return
        for chan in self._inv_obj.channels:
            if isinstance(chan.data_logger, Equipment):
                try:
                    old_gain = chan.data_logger.gain
                    if old_gain == value:
                        continue
                except AttributeError:
                    pass
            else:
                chan.data_logger = Equipment()
            chan.data_logger.gain = value
            if not keep_response:
                chan.response = None
        return True
    dl_gain = property(get_dl_gain, set_dl_gain)
    FM.append(dl_gain)

    def get_dl_type(self):
        #No channels
        channels = self._inv_obj.channels
        if len(channels) < 1:
            return DLS.index('Na')
        try:
            return DLS.index(channels[0].data_logger.type)
        except (AttributeError, ValueError):
            return DLS.index('Na')
    def set_dl_type(self, value):
        for chan in self._inv_obj.channels:
            try:
                chan.data_logger.type = DLS.name(value)
            except AttributeError:
                chan.data_logger = Equipment(type=DLS.name(value))
            chan.response = None
        return True
    dl_type = property(get_dl_type, set_dl_type)
    FM.append(dl_type)

    def get_sensor_sn(self):
        #No channels
        channels = self._inv_obj.channels
        if len(channels) < 1:
            return 'Na'
        try:
            return channels[0].sensor.serial_number
        except AttributeError:
            return 'Na'
    def set_sensor_sn(self, value):
        for chan in self._inv_obj.channels:
            try:
                chan.sensor.serial_number = value
            except AttributeError:
                chan.sensor = Equipment(
                    serial_number = value)
        return True
    sensor_sn = property(get_sensor_sn, set_sensor_sn)
    FM.append(sensor_sn)

    def get_sensor_type(self):
        #No channels
        channels = self._inv_obj.channels
        if len(channels) < 1:
            return SENSORS.index('Na')
        try:
            return SENSORS.index(channels[0].sensor.type)
        except (AttributeError, ValueError):
            return SENSORS.index('Na')
    def set_sensor_type(self, value):
        for chan in self._inv_obj.channels:
            try:
                chan.sensor.type = SENSORS.name(value)
            except AttributeError:
                chan.sensor = Equipment(type=SENSORS.name(value))
            chan.response = None
        return True
    sensor_type = property(get_sensor_type, set_sensor_type)
    FM.append(sensor_type)

    def get_site_name(self):
        return self._inv_obj.site.name
    def set_site_name(self, value):
        self._inv_obj.site.name = value
        return True
    site_name = property(get_site_name, set_site_name)
    FM.append(site_name)

    def get_site_description(self):
        return self._inv_obj.site.description
    def set_site_description(self, value):
        self._inv_obj.site.description = value
        return True
    site_description = property(get_site_description, set_site_description)
    FM.append(site_description)

    def get_site_town(self):
        return self._inv_obj.site.town
    def set_site_town(self, value):
        self._inv_obj.site.town = value
        return True
    site_town = property(get_site_town, set_site_town)
    FM.append(site_town)

    def get_site_county(self):
        return self._inv_obj.site.county
    def set_site_county(self, value):
        self._inv_obj.site.county = value
        return True
    site_county = property(get_site_county, set_site_county)
    FM.append(site_county)

    def get_site_region(self):
        return self._inv_obj.site.region
    def set_site_region(self, value):
        self._inv_obj.site.region = value
        return True
    site_region = property(get_site_region, set_site_region)
    FM.append(site_region)

    def get_site_country(self):
        return self._inv_obj.site.country
    def set_site_country(self, value):
        self._inv_obj.site.country = value
        return True
    site_country = property(get_site_country, set_site_country)
    FM.append(site_country)


class ChannelNode(InventoryNode):
    def __init__(self, name, inv_object, parent):
        InventoryNode.__init__(self, name, inv_object, parent)
        self._inv_obj = inv_object

    FM = InventoryNode.FM
    def get_latitude(self):
        return str(self._inv_obj.latitude)
    def set_latitude(self, value):
        self._inv_obj.latitude = float(value)
        return True
    latitude = property(get_latitude, set_latitude)
    FM.append(latitude)

    def get_longitude(self):
        return str(self._inv_obj.longitude)
    def set_longitude(self, value):
        self._inv_obj.longitude = float(value)
        return True
    longitude = property(get_longitude, set_longitude)
    FM.append(longitude)

    def get_elevation(self):
        return str(self._inv_obj.elevation)
    def set_elevation(self, value):
        self._inv_obj.elevation = float(value)
        return True
    elevation = property(get_elevation, set_elevation)
    FM.append(elevation)

    def get_azimuth(self):
        return str(self._inv_obj.azimuth)
    def set_azimuth(self, value):
        self._inv_obj.azimuth = float(value)
        return True
    azimuth = property(get_azimuth, set_azimuth)
    FM.append(azimuth)

    def get_dip(self):
        return str(self._inv_obj.dip)
    def set_dip(self, value):
        self._inv_obj.dip = float(value)
        return True
    dip = property(get_dip, set_dip)
    FM.append(dip)

    def get_depth(self):
        return str(self._inv_obj.depth)
    def set_depth(self, value):
        self._inv_obj.depth = float(value)
        return True
    depth = property(get_depth, set_depth)
    FM.append(depth)

    def get_location_code(self):
        if type(self) is not ChannelNode:
            return ''
        return self._inv_obj.location_code
    def set_location_code(self, value):
        if type(self) is not ChannelNode:
            return
        self._inv_obj.location_code = value
        return True
    location_code = property(get_location_code, set_location_code)
    FM[InventoryNode.col('LOC')] = location_code

    def get_sample_rate(self):
        if type(self) is not ChannelNode:
            return ''
        return str(self._inv_obj.sample_rate) + 'Hz'
    def set_sample_rate(self, value):
        if type(self) is not ChannelNode:
            return
        value = value.strip('hHzZ')
        self._inv_obj.sample_rate = float(value)
        return True
    sample_rate = property(get_sample_rate, set_sample_rate)
    FM[InventoryNode.col('SR')] = sample_rate

    def get_description(self):
        try:
            return self._inv_obj.sensor.description
        except AttributeError:
            return 'Na'
    def set_description(self, value):
        self._inv_obj.sensor.description = value
        return True
    sensor_description = property(get_description, set_description)
    FM.append(sensor_description)

    def get_comment(self):
        try:
            return self._inv_obj.comments[0].value
        except IndexError:
            return ''
    def set_comment(self, value):
        try:
            self._inv_obj.comments[0].value = value
        except:
            self._inv_obj.comments.append(Comment(value))
        return True
    channel_comment = property(get_comment, set_comment)
    FM.append(channel_comment)

    def plot_response(self):
        if self._inv_obj.sample_rate > 0:
            self._inv_obj.response.plot(1/300)


class InventoryModel(QtCore.QAbstractItemModel):
    def __init__(self, root, parent=None):
        super().__init__(parent)
        self._root_node = root

    def rowCount(self, parent):
        parent_node = self.get_node(parent)
        return parent_node.child_count()

    def columnCount(self, parent):
        return 6

    def data(self, index, role):
        node = self.get_node(index)
        value = node.get_data(index.column())
        if role == QtCore.Qt.DisplayRole:
            # rounded start/end time on left panel
            if index.column() == 3 or index.column() == 4:
                value = QtGui.QFontMetrics(QtGui.QFont()).elidedText(value,
                                                                     QtGui.Qt.ElideRight,
                                                                     100)
                return value
            return value
        if role == QtCore.Qt.EditRole:
            return value
        # show full start/end time as tooltip in mouse hover
        if role == Qt.ToolTipRole:
            if index.column() == 3 or index.column() == 4:
                return value
        if role == QtCore.Qt.BackgroundRole:
            try:
                if node._inv_obj._new:
                    return QtGui.QColor(191, 239, 192, 100)
            except AttributeError:
                return

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        try:
            node = self.get_node(index)
            if role == QtCore.Qt.EditRole:
                if not node.set_data(index.column(), value):
                    return False
                self.dataChanged.emit(index, index)
                return True
                # if index.column() == 0:
                    # node.name = value
                    # self.dataChanged.emit(index, index)
                    # return True
        except Exception as e:
            traceback.print_exc()
            status_message(
                'Failed to set field. See stdout for debugging info.')
        return False

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if section == 0:
                return 'Code'
            elif section == 1:
                return 'Loc'
            elif section == 2:
                return 'SR'
            elif section == 3:
                return 'Start'
            elif section == 4:
                return 'End'
            elif section == 5:
                return 'Response'

    def flags(self, parent):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable \
               | QtCore.Qt.ItemIsEditable

    def index(self, row, column, parent):
        if not parent.isValid():
            parent_node = self._root_node
        else:
            parent_node = parent.internalPointer()
        child = parent_node.child(row)
        if child is not None:
            return self.createIndex(row, column, child)
        else:
            return QtCore.QModelIndex()

    def get_index(self, node):
        return self.createIndex(node.row(), 0, node)

    def parent(self, index):
        if not index.isValid():
            return QtCore.QModelIndex()

        node = index.internalPointer()
        parent = node.parent()
        if parent == self._root_node:
            return QtCore.QModelIndex()
        return self.createIndex(parent.row(), 0, parent)

    def append_node(self, node, parent=QtCore.QModelIndex()):
        position = node.row()
        self.beginInsertRows(parent, position, position)
        #success = parent_node.add_child(node)
        self.endInsertRows()
        #return success

    def remove_row(self, node, parent=QtCore.QModelIndex()):
        row = node.row()
        self.beginRemoveRows(parent, row, row)
        node.remove()
        self.endRemoveRows()

    def clear_data(self, node=None):
        '''
        Removes all rows in model recursively.
        '''
        if node is None:
            node = self._root_node
        while node.child_count() > 0:
            self.clear_data(node.child(0))
        if node is not self._root_node:
            self.remove_row(node, self.parent(self.get_index(node)))

    def add_inventory(self, inventory, net_r=False, stat_r=False, chan_r=False):
        '''
        Replaces data model with new inventory.
        If you want to add and inventory add before and call.
        '''
        self.clear_data()
        # sort networks
        inventory.networks.sort(key=lambda x: x.code, reverse=net_r)
        for net in inventory.networks:
            # Making a node inserts it so append_node is empty
            net_node = NetworkNode(net.code, net, self._root_node)
            self.append_node(net_node)
            net_index = self.get_index(net_node)
            # sort stations
            net.stations.sort(key=lambda x: x.code, reverse=stat_r)
            for sta in net.stations:
                sta_node = StationNode(sta.code, sta, net_node)
                self.append_node(sta_node, net_index)
                sta_index = self.get_index(sta_node)
                # sort channels
                sta.channels.sort(key=lambda x: x.code, reverse=chan_r)
                for chan in sta.channels:
                    chan_node = ChannelNode(chan.code, chan, sta_node)
                    self.append_node(chan_node, sta_index)

    def get_node(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node
        return self._root_node

class NexusWindow(*load_ui("NexusWindow.ui")):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        # default sort is ascending
        self.sorting_dict = {
            NetworkNode: False,
            StationNode: False,
            ChannelNode: False
        }

        # Quick aliases
        global status_message
        status_message = self.status_message
        self.update_gui = QtWidgets.QApplication.processEvents

        self.root_node = InventoryNode('root')
        self.inv_model = InventoryModel(self.root_node, self)
        self.uiInventoryTree.setModel(self.inv_model)
        self.uiInventoryTree.setSelectionMode(
            QtWidgets.QAbstractItemView.ExtendedSelection)
        self.selector = self.uiInventoryTree.selectionModel()

        self.inventory = obspyImproved.InventoryIm()
        # Indicates if a new load should be marked new
        self._new = False

        self.editor = EditorWiget(self)
        self.uiSplitter.addWidget(self.editor)
        self.editor.setModel(self.inv_model)

        self.uiInventoryTree.selectionModel().currentChanged.connect(
            self.editor.setSelection)
        self.uiInventoryTree.setContextMenuPolicy(Qt.CustomContextMenu)
        self.uiInventoryTree.customContextMenuRequested.connect(
            self.sorting_popup)

        # Actions
        self.actionScan_MiniSEED.triggered.connect(self.scan_ms_dialog)
        self.actionWrite_StationXML.triggered.connect(self.write_xml_dialog)
        self.actionRead_StationXML.triggered.connect(self.read_xml_dialog)
        self.actionPlotStations.triggered.connect(self.plot_stations)
        self.actionCalculateResponses.triggered.connect(
            self.calculate_responses)

        self.set_ui_nrl_root()
        self.actionNRL_online.triggered.connect(self.nrl_online_toggled)
        self.actionNRL_root.triggered.connect(self.nrl_root_local_dialog)
        # Buttons
        self.uiSplitButton_epoch.clicked.connect(self.split_by_epoch)
        self.uiSplitButton_chan.clicked.connect(self.split_by_chan)
        self.uiAddButton.clicked.connect(self.add)
        self.uiSubtractButton.clicked.connect(self.subtract)

        # test
        self.actionDebug.triggered.connect(self.debug)

    def sorting_popup(self, point):
        '''
        Bring up menu to let user sort
        inventory in ascending/descending order
        '''
        index = self.uiInventoryTree.indexAt(point)
        node = self.inv_model.get_node(index)
        node_type = ''
        if isinstance(node, NetworkNode):
            node_type = 'Networks'
        elif isinstance(node, StationNode):
            node_type = 'Stations'
        elif isinstance(node, ChannelNode):
            node_type = 'Channels'
        menu = QtWidgets.QMenu(self)
        sort_asc = QtGui.QAction(f"Sort {node_type} 0-Z")
        sort_asc.triggered.connect(lambda: self.set_sorting_order('ascending', type(node)))
        sort_desc = QtGui.QAction(f"Sort {node_type} Z-0")
        sort_desc.triggered.connect(lambda: self.set_sorting_order('descending', type(node)))
        menu.exec([sort_asc, sort_desc], self.uiInventoryTree.mapToGlobal(point))

    def set_sorting_order(self, order, node):
        '''
        Repopulate inventory in ascending or
        descending order
        '''
        sort_value = False if order == 'ascending' else True
        self.sorting_dict[node] = sort_value
        # keep scroll bar where it was after
        # sorting inventory
        scroll_value = self.uiInventoryTree.verticalScrollBar().value()
        self.status_message('Sorting...')
        self.inv_model.add_inventory(
            self.inventory,
            self.sorting_dict[NetworkNode],
            self.sorting_dict[StationNode],
            self.sorting_dict[ChannelNode])
        self.reshape_tree()
        self.uiInventoryTree.verticalScrollBar().setValue(scroll_value)
        self.status_message('Sorting... Done.')

    def debug(self):
        pass

    def set_ui_nrl_root(self):
        self.actionNRL_root.setText(hardware.NRL_ROOT)

    def nrl_online_toggled(self):
        sender = self.sender()
        if sender.isChecked():
            self.NRL_online = True
            hardware.NRL_ROOT = hardware.NRL_URL
            self.set_ui_nrl_root()
        else:
            self.NRL_online = False
            self.nrl_root_local_dialog()

    def nrl_root_local_dialog(self):
        dir_name = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            'Local NRL root:')
        if dir_name:
            hardware.NRL_ROOT = dir_name
            self.set_ui_nrl_root()
            self.actionNRL_online.setChecked(False)
        else:
            self.actionNRL_online.setChecked(True)

    def status_message(self, message):
        self.statusBar().showMessage(message)
        self.update_gui()

    def reload_data_changed(self):
        '''Lazy way to have the datatable reload'''
        self.inv_model.layoutAboutToBeChanged.emit()
        self.inv_model.layoutChanged.emit()

    def add(self):
        item = self.get_inv_obj_from_selection()
        self.inventory.clear_new()
        if isinstance(item, Network):
            self.inventory.new_station(item)
        elif isinstance(item, Station):
            self.inventory.new_channel(item)
        else:
            self.inventory.new_network()
        self.inv_model.add_inventory(self.inventory)
        self.reshape_tree()

    def subtract(self):
        items = self.get_inv_obj_from_selection('subtract')
        for item in items:
            i = item.model().get_node(item)._inv_obj
            self.inventory.delete(i)
        self.inv_model.add_inventory(self.inventory)
        self.reshape_tree()

    def get_inv_obj_from_selection(self, caller=''):
        index_list = self.selector.selectedRows()
        if caller == 'subtract':
            return index_list
        else:
            if len(index_list) != 1:
                return
            index = index_list[0]
            return index.model().get_node(index)._inv_obj


    def calculate_responses(self):
        self.status_message('Calculating responses...')
        self.inventory.calculate_responses()
        self.status_message('Calculating responses... Done.')
        self.reload_data_changed()

    def split_by_epoch(self):
        # indexes_selected = self.selector.selectedRows()
        # print(indexes_selected)
        # for i in indexes_selected:
            # print(i.row())
            # print(i.model().get_node(i))
            # print(i.model())

        # # Only Stations can be split
        # station = i.model().get_node(i)._inv_obj
        station = self.get_inv_obj_from_selection()
        if not isinstance(station, Station):
            self.status_message('Only stations can be split.')
            return

        # Get time to split
        middle = obspyImproved.utc_to_str(
            self.inventory.middle_epoch(station))
        text, ok = QtWidgets.QInputDialog.getText(self,
                                                  'Split station',
                                                  'Split time:',
                                                  text=middle)
        if ok:
            time = obspyImproved.utc_from_str(text)
        else:
            return


        # Split the inventory
        self.inventory.split_station_by_epoch(station, time=time)

        # Recreate editor
        self.inv_model.add_inventory(self.inventory)

        #self.reshape_tree()

        return

    def split_by_chan(self):
        station = self.get_inv_obj_from_selection()
        if not isinstance(station, Station):
            self.status_message('Only stations can be split.')
            return
        dialog = ChannelSelectDialog(parent=self, station=station)
        if dialog.exec_():
            self.inventory.split_station_by_channels(
                station,
                dialog.selected_channels,
                )
            self.inv_model.add_inventory(self.inventory)

    def scan_ms_dialog(self):
        dir_name = QtWidgets.QFileDialog.getExistingDirectory(self, 'Dir to scan for MSEED')
        if not dir_name:
            return
        # For testing
        # dir_name = '.'
        self.status_message('Scanning {}...'.format(dir_name))

        #num_mseed = self.inventory.scan_slow(
        num_mseed = self.inventory.scan_quick(
            dir_name, new=self._new, log_message=self.status_message)
        self.status_message('Scanning {}... Done. '
                            'Found {} MS files.'.format(
                                dir_name, num_mseed))
        if self._new is False:
            self.inventory.clear_new()
        self._new = True
        self.inv_model.add_inventory(self.inventory)
        self.reshape_tree()

    def read_xml_dialog(self):
        # file_name, __ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open StationXML')
        # testing
        file_name = './test/data/ANDIVOLC.xml'
        file_name, __ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open StationXML', './test/data')

        if file_name != '':
            self.status_message('Opening StationXML {}...'.format(file_name))
            try:
                #new_inventory = obspy.read_inventory(file_name, format='STATIONXML')
                new_inventory = read_inventory(file_name)
                new_inventory = obspyImproved.InventoryIm() + new_inventory
                if self._new:
                    new_inventory.set_new()
                self._new = True
                self.inventory.clear_new()
                self.inventory += new_inventory
                #self.inv_model.beginResetModel()
                self.inv_model.add_inventory(self.inventory)
                #self.inv_model.endResetModel()
                self.reshape_tree()
                self.status_message('Opening StationXML {}... Done.'.format(
                    file_name))
                self._new = True
                return True
            except Exception as e:
                self.status_message('Opening StationXML {}... Failed.'.format(
                    file_name))
                print(e)
                print('Failed to read {} as StationXML.'.format(file_name))
                return False
        else:
            self.status_message('Opening StationXML {}... Bad filename'.format(
                file_name))


    def plot_stations(self):
        try:
            self.status_message('Plotting Stations...')
            #self.inventory.plot()
            self.inventory.plot(projection='ortho')
            #self.inventory.plot(projection='local')
        except Exception as e:
            self.status_message('Plotting Stations... '
                                         'Failed. {}.'.format(e))
            print(e)
            return
        self.status_message('Plotting Stations... Done.')

    def write_xml_dialog(self):
        file_name, __ = QtWidgets.QFileDialog.getSaveFileName(self, 'Save '
                                                          'StationXML')
        if file_name == '':
            self.status_message('Saving{}... Canceled'.format(file_name))
            return
        self.status_message('Saving{}...'.format(file_name))
        self.inventory.write(file_name, format='STATIONXML')
        self.status_message('Saving{}...Done.'.format(file_name))

    def reshape_tree(self):
        """
        Expands entire tree and makes contents fit columns
        """
        self.uiInventoryTree.expandAll()
        for col in range(self.inv_model.columnCount(None)):
            self.uiInventoryTree.resizeColumnToContents(col)
        self.uiInventoryTree.header().setSectionResizeMode(QHeaderView.Interactive)
    
    def closeEvent(self, event):
        """ 
            Generate 'question' dialog on clicking 'X' button in title bar.
            Reimplement the closeEvent() event handler to include a 'Question'
            dialog with options on how to proceed - Save, Close, Cancel buttons.
            Save opens the write to xml dialog.
            Close closes the window without saving.
            Cancel closes the exit dialog and continues to run.
        """
        reply = QMessageBox.question(
            self, "Message",
            "Are you sure you want to quit? Any unsaved work will be lost.",
            QMessageBox.Save | QMessageBox.Close | QMessageBox.Cancel,
            QMessageBox.Save)

        if reply == QMessageBox.Close:
            event.accept()
        elif reply == QMessageBox.Cancel:
            event.ignore()
        else:
            self.write_xml_dialog()
            event.accept()

        
    def keyPressEvent(self, event):
        """
            Close application from escape key.
            results in QMessageBox dialog from closeEvent, good but how/why?
        """
        if event.key() == Qt.Key_Escape:
            self.close()
    


class EditorWiget(*load_ui("EditorWidget.ui")):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        #self.inv_model = None

        self.network_gen_editor = NetworkGenWidget(self)
        self.station_gen_editor = StationGenWidget(self)
        self.channel_gen_editor = ChannelGenWidget(self)
        self.network_editor = DescriptionWidget(self)
        self.station_editor = StationWidget(self)
        self.channel_editor = ChannelWidget(self)

        self.uiGeneral.addWidget(self.network_gen_editor)
        self.uiGeneral.addWidget(self.station_gen_editor)
        self.uiGeneral.addWidget(self.channel_gen_editor)
        self.uiSpecific.addWidget(self.network_editor)
        self.uiSpecific.addWidget(self.station_editor)
        self.uiSpecific.addWidget(self.channel_editor)

        self.network_gen_editor.setVisible(False)
        self.station_gen_editor.setVisible(False)
        self.channel_gen_editor.setVisible(False)

        self.network_editor.setVisible(False)
        self.station_editor.setVisible(False)
        self.channel_editor.setVisible(False)

    def setModel(self, model):
        self.inv_model = model
        self.network_gen_editor.setModel(self.inv_model)
        self.station_gen_editor.setModel(self.inv_model)
        self.channel_gen_editor.setModel(self.inv_model)
        self.network_editor.setModel(self.inv_model, NetworkNode)
        self.station_editor.setModel(self.inv_model)
        self.channel_editor.setModel(self.inv_model)

    def setSelection(self, current, old):
        node = current.internalPointer()
        if node is not None:
            self.network_gen_editor.setVisible(True)
            if isinstance(node, ChannelNode):
                # General top Editors
                self.station_gen_editor.setVisible(True)
                self.channel_gen_editor.setVisible(True)
                # Specific editors
                self.network_editor.setVisible(False)
                self.station_editor.setVisible(False)
                self.channel_editor.setVisible(True)
                # Selections Specific
                self.channel_editor.setSelection(current, old)
                # Selections General
                self.channel_gen_editor.setSelection(current, old)
                sta = current.parent()
                self.station_gen_editor.setSelection(sta, old)
                net = sta.parent()
                self.network_gen_editor.setSelection(net, old)
            elif isinstance(node, StationNode):
                # General top Editors
                self.channel_gen_editor.setVisible(False)
                self.station_gen_editor.setVisible(True)
                # Specific editors
                self.network_editor.setVisible(False)
                self.channel_editor.setVisible(False)
                self.station_editor.setVisible(True)
                # Selections
                self.station_editor.setSelection(current, old)
                self.station_gen_editor.setSelection(current, old)
                net = current.parent()
                self.network_gen_editor.setSelection(net, old)
            elif isinstance(node, NetworkNode):
                # General top editors
                self.station_gen_editor.setVisible(False)
                self.channel_gen_editor.setVisible(False)
                # Specific editors
                self.station_editor.setVisible(False)
                self.channel_editor.setVisible(False)
                self.network_editor.setVisible(True)
                # Selections
                self.network_editor.setSelection(current, old)
                self.network_gen_editor.setSelection(current, old)
            else:
                self.network_gen_editor.setVisible(False)
                self.station_gen_editor.setVisible(False)
                self.channel_gen_editor.setVisible(False)
                self.network_editor.setVisible(False)
                self.station_editor.setVisible(False)
                self.channel_editor.setVisible(False)


class WidgetBase():
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.setupUi(self)

    def setModel(self, model):
        self._model = model
        self._data_mapper = QtWidgets.QDataWidgetMapper()
        self._data_mapper.setModel(model)

    def map_code_start_end(self):
        n = InventoryNode
        self._data_mapper.addMapping(self.uiCode, n.col(n.code))
        self._data_mapper.addMapping(self.uiStart, n.col(n.start))
        self._data_mapper.addMapping(self.uiEnd, n.col(n.end))

    def setSelection(self, current, old):
        parent = current.parent()
        self._data_mapper.setRootIndex(parent)
        self._data_mapper.setCurrentIndex(current.row())


class NetworkGenWidget(WidgetBase, *load_ui("NetworkGenWidget.ui")):
    def __init__(self, parent=None):
        super().__init__(parent)

    def setModel(self, model):
        super().setModel(model)
        self.map_code_start_end()


class StationGenWidget(WidgetBase, *load_ui("StationGenWidget.ui")):
    def __init__(self, parent=None):
        super().__init__(parent)

    def setModel(self, model):
        super().setModel(model)
        self.map_code_start_end()


class ChannelGenWidget(WidgetBase, *load_ui("ChannelGenWidget.ui")):
    def __init__(self, parent=None):
        super().__init__(parent)

    def setModel(self, model):
        super().setModel(model)
        self.map_code_start_end()
        n = ChannelNode
        self._data_mapper.addMapping(self.uiLocationCode, n.col(n.location_code))
        self._data_mapper.addMapping(self.uiSampleRate, n.col(n.sample_rate))

class DescriptionWidget(WidgetBase, *load_ui("DescriptionWidget.ui")):
    def __init__(self, parent=None):
        super().__init__(parent)

    def setModel(self, model, node_type):
        super().setModel(model)
        n = node_type
        self._data_mapper.addMapping(self.uiDescription, n.col(n.description))


class StationWidget(WidgetBase, *load_ui("StationWidget.ui")):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.build_dl_combo()
        self.copyDL_button.clicked.connect(self.copy_dltype_and_gain)
        self.copySensor_button.clicked.connect(self.copy_sensor)
        self.uiDLType.activated.connect(self.combo_changed)
        self.build_sensor_combo()
        self.uiSensorType.activated.connect(self.combo_changed)
        self.uiDLType.currentIndexChanged.connect(self.update_model)
        self.uiSensorType.currentIndexChanged.connect(self.update_model)
        self.uiDLGain.editingFinished.connect(self.update_model)

    def update_model(self):
        self._model.layoutAboutToBeChanged.emit()
        self._model.layoutChanged.emit()

    def build_dl_combo(self):
        # Remove all items
        for i in reversed(range(self.uiDLType.count())):
            self.uiDLType.removeItem(i)
        # Rebuild
        for dl in DLS.names():
            self.uiDLType.addItem(dl)
        self.uiDLType.addItem(ELIPSES)

    def build_sensor_combo(self):
        # Remove all items
        for i in reversed(range(self.uiSensorType.count())):
            self.uiSensorType.removeItem(i)
        # Rebuild
        for sensor in SENSORS.names():
            self.uiSensorType.addItem(sensor)
        self.uiSensorType.addItem(ELIPSES)


    # Hack as datamapper wont call setData for comboboxes
    def combo_changed(self, value):
        parent = self._data_mapper.rootIndex()
        row = self._data_mapper.currentIndex()
        sender_name = self.sender().objectName()
        if sender_name == 'uiDLType':
            column = StationNode.col(StationNode.dl_type)
            if self.sender().itemText(value) == ELIPSES:
                # Add DL
                try:
                    dialog = NRLWizard(DATALOGGER, root=hardware.NRL_ROOT)
                    if dialog.exec_():
                        DLS.add_from_nrl_gui(dialog)
                        self.build_dl_combo()
                        value = DLS.index(dialog.nickname)
                    else:
                        value = DLS.index('Na')
                except Exception as e:
                    status_message('Failed to pick response. See stdout.')
                    print(e)
                    value = DLS.index('Na')

        elif sender_name == 'uiSensorType':
            column = StationNode.col(StationNode.sensor_type)
            if self.sender().itemText(value) == ELIPSES:
                # Add sensor
                try:
                    dialog = NRLWizard(SENSOR, root=hardware.NRL_ROOT)
                    if dialog.exec_():
                        SENSORS.add_from_nrl_gui(dialog)
                        self.build_sensor_combo()
                        value = SENSORS.index(dialog.nickname)
                    else:
                        value = SENSORS.index('Na')
                except Exception as e:
                    status_message('Failed to pick response. See stdout.')
                    value = SENSORS.index('Na')

        index = self._model.index(row, column, parent)
        self._model.setData(index, value)

    def setModel(self, model):
        super().setModel(model)
        n = StationNode
        self._data_mapper.addMapping(self.uiLatitude, n.col(n.latitude))
        self._data_mapper.addMapping(self.uiLongitude, n.col(n.longitude))
        self._data_mapper.addMapping(self.uiElevation, n.col(n.elevation))
        self._data_mapper.addMapping(self.uiDepth, n.col(n.depth_station))
        self._data_mapper.addMapping(self.uiDLSN, n.col(n.dl_sn))
        self._data_mapper.addMapping(self.uiDLGain, n.col(n.dl_gain))
        self._data_mapper.addMapping(self.uiDLType, n.col(n.dl_type),
                                     bytes('currentIndex','ascii'))

        self._data_mapper.addMapping(self.uiSensorSN, n.col(n.sensor_sn))
        self._data_mapper.addMapping(self.uiSensorType, n.col(n.sensor_type),
                                     bytes('currentIndex', 'ascii'))
        self._data_mapper.addMapping(self.uiSiteName, n.col(n.site_name))
        self._data_mapper.addMapping(self.uiSiteDescription, n.col(n.site_description))
        self._data_mapper.addMapping(self.uiSiteTown, n.col(n.site_town))
        self._data_mapper.addMapping(self.uiSiteCounty, n.col(n.site_county))
        self._data_mapper.addMapping(self.uiSiteRegion, n.col(n.site_region))
        self._data_mapper.addMapping(self.uiSiteCountry, n.col(n.site_country))

    def copy_dltype_and_gain(self):
        stations = self.get_stations()
        self.copy_to_stations(stations,
                              'type & gain',
                              [self.uiDLGain.text(), self.uiDLType.currentText()])
    
    def copy_sensor(self):
        stations = self.get_stations()
        self.copy_to_stations(stations,
                              'sensor',
                              [self.uiSensorType.currentText()])
    
    def copy_to_stations(self, stations, action, values):
        dialog = StationSelectDialog(stations, action, values, parent=self)
        if dialog.exec_():
            if dialog.uiSensorLabel.parent() is None:
                copy_this = {"DLtype": dialog.uiTypeCB.isChecked(),
                             "DLgain": dialog.uiGainCB.isChecked()}
            else:
                copy_this = {"Stype": True}
            self.update_stations(dialog.selected_stations, copy_this)

    def update_stations(self, stations, copy_this):
        for station in stations:
            for node, obj in self.stat_node_to_obj_map.items():
                if station == obj:
                    for key, value in copy_this.items():
                        if value is False:
                            continue
                        if key == "DLtype":
                            node.set_dl_type(self.uiDLType.currentIndex())
                        elif key == "DLgain":
                            node.set_dl_gain(self.uiDLGain.text())
                        else:
                            node.set_sensor_type(self.uiSensorType.currentIndex())

    def get_stations(self):
        """
        Get all stations from inventory to
        add to Station Select Dialogue
        """
        # get top most network
        net = self._model.index(0, 0, QtCore.QModelIndex())
        self.stat_node_to_obj_map = {}
        stations = []
        net_row = 0
        try:
            while True:
                for r in range(self._model.rowCount(net)):
                    station_index = self._model.index(r, 0, net)
                    station_node = station_index.model().get_node(station_index)
                    station_obj = station_node._inv_obj
                    stations.append(station_obj)
                    self.stat_node_to_obj_map[station_node] = station_obj
                net_row += 1
                net = self._model.sibling(net_row, 0, net)
        except IndexError:
            return stations


class ChannelWidget(WidgetBase, *load_ui("ChannelWidget.ui")):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.uiChannelTypeCombo.addItem('GEOPHYSICAL')
        self.uiChannelTypeCombo.addItem('HEALTH')

    def setModel(self, model):
        super().setModel(model)
        n = ChannelNode
        self._data_mapper.addMapping(self.uiAzimuth, n.col(n.azimuth))
        self._data_mapper.addMapping(self.uiDip, n.col(n.dip))
        self._data_mapper.addMapping(self.uiDepth, n.col(n.depth))
        self._data_mapper.addMapping(self.uiComment, n.col(n.channel_comment))
        self._data_mapper.addMapping(self.uiSensorDesc, n.col(n.sensor_description))
        self._data_mapper.addMapping(self.uiLatitude, n.col(n.latitude))
        self._data_mapper.addMapping(self.uiLongitude, n.col(n.longitude))
        self._data_mapper.addMapping(self.uiElevation, n.col(n.elevation))

    def setSelection(self, current, old):
        super().setSelection(current, old)
        try:
            # There is difference between Python and C++ version of disconnect
            # In order to make sure that the disconnect function is executed
            # correctly, Three arguments are needed to be passed to disconnect
            # function.
            self.uiPlotResponseButton.disconnect(None, None, None)
        except TypeError:
            pass
        node = current.internalPointer()
        if not node.has_response:
            self.uiPlotResponseButton.setDisabled(True)
            self.uiPlotResponseLabel.setEnabled(False)
        else:
            self.uiPlotResponseButton.setDisabled(False)
            self.uiPlotResponseLabel.setEnabled(True)
            self.uiPlotResponseButton.clicked.connect(node.plot_response)


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = NexusWindow()
    window.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
