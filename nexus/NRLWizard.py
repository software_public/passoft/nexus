#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Lloyd Carothers
Qt Wizard for selecting responses from the IRIS NRL
'''

from PySide6 import QtWidgets

from obspy.clients.nrl import NRL

SENSOR = object()
DATALOGGER = object()

SR = 'sr'
GAIN = 'gain'


class NRLWizard(QtWidgets.QWizard):
    def __init__(self, equipment=SENSOR, root=None):
        super().__init__()
        #Hack for now as NRL(root=None) crashes
        if not root:
            self.nrl = NRL()
        else:
            self.nrl = NRL(root=root)
        nrl_root = self.nrl.root

        if equipment is SENSOR:
            self.top = self.nrl.sensors
            self.setWindowTitle('Select a sensor from NRL {}'.format(
                nrl_root))
        elif equipment is DATALOGGER:
            self.top = self.nrl.dataloggers
            self.setWindowTitle('Select a datalogger from NRL {}'.format(
                nrl_root))
        else:
            raise ValueError
        self.q_and_as = []
        self.addPage(NRLPage(self, self.top))

    def fresh_page(self):
        # remove all pages ahead
        current_id = self.currentId()
        for id in self.pageIds():
            if id > current_id:
                self.removePage(id)
        #build next page
        question = self.top
        for answer in (a for q, a in self.q_and_as):
            question = question[answer]
        self.addPage(NRLPage(self, question))


class NRLPage(QtWidgets.QWizardPage):
    def __init__(self, parent, nrl_dict):
        super().__init__(parent)
        self.parent = parent
        self.nrl_dict = nrl_dict

    def initializePage(self):
        self.setLayout(QtWidgets.QVBoxLayout())
        
        scroll_area = QtWidgets.QScrollArea(self)
        self.layout().addWidget(scroll_area)
        scroll_area.setWidgetResizable(True)
        
        inner = QtWidgets.QFrame(scroll_area)
        layout = QtWidgets.QVBoxLayout(inner)
        inner.setLayout(layout)
        scroll_area.setWidget(inner)
        
        self.final = False
        # Last page
        if isinstance(self.nrl_dict, tuple):
            self.final = self.nrl_dict
            layout.addWidget(QtWidgets.QLabel(str(self.nrl_dict)))
            layout.addWidget(QtWidgets.QLabel('Name:'))
            self.nickname = QtWidgets.QLineEdit()
            layout.addWidget(self.nickname)
            self.nickname.setText(self.nrl_dict[0])
        # q and a
        else:
            self.selected_radio = None
            question = self.nrl_dict._question
            q = QtWidgets.QLabel(question + ':', self)
            layout.addWidget(q)
            for answer in sorted(self. nrl_dict.keys()):
                radio = QtWidgets.QRadioButton(answer)
                radio.clicked.connect(self.on_selected)
                radio.q_and_a = (question, answer)
                layout.addWidget(radio)

            # Make a dummy page for next button instead of Done.
            self.parent.addPage(NRLPage(self.parent, None))

    def on_selected(self):
        self.selected_radio = self.sender()

    def validatePage(self):
        if self.final:
            self.parent.final = self.final
            self.parent.nickname = self.nickname.text()
            return 1
        elif self.selected_radio:
            self.parent.q_and_as.append(self.selected_radio.q_and_a)
            self.parent.fresh_page()
            return 1
        else:
            return 0

    def cleanupPage(self):
        try:
            self.parent.q_and_as.pop()
        except IndexError:
            pass
