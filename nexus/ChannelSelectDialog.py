#!/usr/bin/env python
# -*- coding: utf-8 -*-*#
"""
Dialog for selecting channels from a station
Lloyd Carothers
IRIS/PASSCAL
"""

import os

from PySide6.QtCore import Qt
from PySide6.QtGui import QFontMetrics
from PySide6.QtUiTools import loadUiType
from PySide6.QtWidgets import (QTableWidgetItem, QAbstractItemView,
                               QHeaderView)

from .obspyImproved import utc_to_str

def load_ui(filename):
    """
    Helper function
    Load a ui file relative to this source file
    """
    path = os.path.join(os.path.dirname(__file__), filename)
    try:
            ret = loadUiType(path)
    except Exception as e:
        print(e)
        raise e
    return ret


class ChannelSelectDialog(*load_ui('ChannelSelectDialog.ui')):
    def __init__(self, station, parent=None):
        self.station = station
        self.chan_map = {}
        super().__init__(parent)
        self.setupUi()

    def accept(self):
        rows = set(item.row() for item in self.tableWidget.selectedItems())
        self.selected_channels = []
        for row in rows:
            self.selected_channels.append(self.chan_map[row])
        super().accept()

    def sort_table(self, column):
        """
        Sort Channel Select Dialog table based on column
        header selected by user.
        """
        row_map = {0: 'location_code', 1: 'code', 2: 'sample_rate',
                   3: 'start_date', 4: 'end_date'}
        order = self.tableWidget.horizontalHeader().sortIndicatorOrder()
        descending = False if order == Qt.AscendingOrder else True
        self.chan_map = {ind: v for ind, (_, v) in enumerate(sorted(self.chan_map.items(),
                                                                    key=lambda item: getattr(item[1], row_map[column]),
                                                                    reverse=descending))}

    def setupUi(self):
        super().setupUi(self)
        self.setWindowTitle('Select channels')
        self.label.setText('Station: {}'.format(self.station.code))
        self.tableWidget.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tableWidget.setRowCount(len(self.station))
        self.tableWidget.horizontalHeader().sectionClicked.connect(self.sort_table)
        # Add channels
        for row, chan in enumerate(self.station):
            for col, value in enumerate(
                    ('{:>2s}'.format(chan.location_code),
                     '{:<3s}'.format(chan.code),
                     '{:>6.1f}'.format(chan.sample_rate),
                     utc_to_str(chan.start_date),
                     utc_to_str(chan.end_date),
                     )):
                item = QTableWidgetItem()
                item.setText(value)
                # shorten start/end time
                if col == 3 or col == 4:
                    # full time as tooltip
                    item.setToolTip(value)
                    # rounded time in table
                    value = QFontMetrics(self.tableWidget.font()).elidedText(value,
                                                                             Qt.ElideRight,
                                                                             100)
                    item.setText(value)
                self.tableWidget.setItem(row, col, item)
            self.chan_map[row] = chan
        assert True
        self.tableWidget.resizeColumnsToContents()
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
