# -*- coding: utf-8 -*-

"""Top-level package for nexus."""

__author__ = """IRIS PASSCAL"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2023.4.6.3'
