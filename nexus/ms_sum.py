#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Lloyd Carothers
ms_sum
prints a sync like output ignoring all gaps
'''
import os, sys
from obspy.io.mseed.core import _is_mseed
from obspy.io.mseed import util

from .obspyImproved import InventoryIm


def main():
    inv = InventoryIm()
    for path in sys.argv[1:]:
        if os.path.isfile(path) and _is_mseed(path):
            with open(path, 'rb') as fp:
                rec = util.get_record_information(fp)
                endtime = util.get_start_and_end_time(fp)
            rec['endtime'] = endtime[1]
            inv.add_from_record_info(rec)
        elif os.path.isdir(path):
            inv.scan_quick(path,log_message=lambda x:None)
    inv.print_sum()

