#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Lloyd Carothers
Sensor and datalogger classes
'''
from collections import OrderedDict
from enum import Enum
import os.path
import pickle

from obspy import Inventory, read_inventory
from obspy.core.inventory import Response
from obspy.clients.nrl import NRL

NRL_URL = 'http://ds.iris.edu/NRL'
NRL_ROOT = NRL_URL

current_dir = os.path.dirname(os.path.abspath(__file__))
etc_dir = os.path.join(current_dir, 'etc')
resp_dir = os.path.join(etc_dir, 'SOH_RESP')
defaultsfile = os.path.join(etc_dir,'nexus.hardware')


def chan_name_is_geophysical(channel_code):
    '''
    Returns true if channel code is determined to be a waveform which response
    should be fetched from the NRL i.e. Not a SOH channel.
    '''
    band = channel_code[0]
    instrument = channel_code[1]
    orientation = channel_code[2]
    
    # Band codes from SEED Manual excluding A & O
    if band in 'FGDCESHBMLVURPTQ':
        # Seismic instrument codes
        if instrument in 'HLGNP':
            if orientation in 'ZNE12':
                return True
        # Infrasound / pressure / hydrophone
        if instrument in 'D':
            if orientation in 'OIDFHU':
                return True
    return False


class PlaceHolder(Enum):
    GAIN = 1
    SR = 2


class Hardware(object):
    def __init__(self, name=''):
        self.name = name
        self.NRL_keys = ()


class Sensor(Hardware):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.z_is_up = True

    def __repr__(self):
        s =  f'{self.name} {self.NRL_keys}\n'
        if self.z_is_up:
            s += '\tVertical Up (Broadband)\n'
        else:
            s += '\tVertical Down (Industry)\n'
        return s

    def get_dip(self, component):
        if component == 'Z':
            if self.z_is_up:
                return -90.00
            else:
                return 90.0
        elif component in ('N', '1', 'E', '2'):
            return 0.0
        else:
            return None

    def get_azimuth(self, component):
        if component in ('Z', 'N', '1'):
            return 0.0
        elif component in ('E', '2'):
            return 90.0

    def get_nrl_keys(self):
        return self.NRL_keys

    def from_gui(self, widget):
        answer_list = (a for q, a in widget.q_and_as)
        self.NRL_keys = tuple(answer_list)


class DataLogger(Hardware):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.soh_resps  = {}

    def __repr__(self):
        s =  f'{self.name} {self.get_nrl_keys("GAIN", "SR")}\n'
        for chan, resp in self.soh_resps.items():
            s += f'\t{chan}\n\t\t{resp}\n'
        return s

    def requires_gain(self):
        if PlaceHolder.GAIN in self.NRL_keys:
            return True
        else:
            return False

    def requires_sr(self):
        if PlaceHolder.SR in self.NRL_keys:
            return True
        else:
            return False

    def get_soh_resps(self, code):
        if code[:2] == 'VM':
            return self.soh_resps['VM']
        else:
            return self.soh_resps[code]

    def get_nrl_keys(self, gain=None, sr=None):
        keys = []
        for ans in self.NRL_keys:
            if ans is PlaceHolder.GAIN:
                if not gain:
                    raise TypeError('Gain required')
                ans = gain
            elif ans is PlaceHolder.SR:
                if not sr:
                    raise TypeError('SR required')
                ans = sr
            keys.append(ans)
        return keys

    def from_gui(self, widget):
        answer_list = list()
        for q, a in widget.q_and_as:
            if q.find('gain') >= 0 and a.isdigit():
                answer_list.append(PlaceHolder.GAIN)
            elif q.find('sample rate') >= 0:
                answer_list.append(PlaceHolder.SR)
            else:
                answer_list.append(a)
        self.NRL_keys = tuple(answer_list)


class NrlDict(OrderedDict):
    def __init__(self, container_type=None):
        super().__init__(self)
        self.type = container_type
    def append(self, item):
        assert isinstance(item, Hardware)
        self[item.NRL_keys] = item
    def names(self):
        # returns all the names in dict
        return [item.name for item in self.values()]
    def index_name(self):
        # returns (index, name) of all in dict
        return [ (list(self.keys()).index(key), self[key].name) for key in self.keys()]
    def index(self, name):
        # returns index from name
        return self.names().index(name)
    def name(self, index):
        # returns name from index replaces DLS[index]
        return self.names()[index]
    def get(self, name):
        # returns hardware object from name
        return self[self.keys_from_name(name)]
    def keys_from_name(self, name):
        return [key for key, item in self.items() if item.name == name][0]
    def add_from_nrl_gui(self, widget):
        name = widget.nickname
        o = self.type(name)
        o.from_gui(widget)
        self.append(o)
        save()

    def __repr__(self):
        s = 'Hardware:\n'
        for o in self.values():
            s += o.__repr__()
        return s

def get_nrl_response(dl_keys, sensor_keys):
    nrl = NRL(NRL_ROOT)
    return nrl.get_response(dl_keys, sensor_keys)

sensors = NrlDict(Sensor)
dataloggers = NrlDict(DataLogger)

def save(filename = defaultsfile):
    with open(filename, 'wb') as f:
        pickle.dump(sensors, f)
        pickle.dump(dataloggers, f)

def load(filename = defaultsfile):
    if os.path.isfile(filename):
        with open(filename, 'rb') as f:
            global sensors
            global dataloggers
            sensors = pickle.load(f)
            dataloggers = pickle.load(f)
    else:
        create_defaults()

def create_defaults():
    s = Sensor('Na')
    sensors.append(s)
    s = Sensor('CMG-3T')
    s.NRL_keys = ('Guralp', 'CMG-3T', '120s - 50Hz', '1500')
    sensors.append(s)
    s = Sensor('Trillium 40')
    s.NRL_keys = ('Nanometrics', 'Trillium 40')
    sensors.append(s)
    s = Sensor('L-22')
    s.NRL_keys = ('Sercel/Mark Products', 'L-22D', '5470 Ohms', '20000 Ohms')
    sensors.append(s)
    s = Sensor('STS-2 gen1')
    s.NRL_keys = ('Streckeisen', 'STS-2', '1500', '1 - installed 01/90 to 09/94')
    sensors.append(s)
    s = Sensor('STS-2 gen2')
    s.NRL_keys = ('Streckeisen', 'STS-2', '1500', '2 - installed 09/94 to 04/97')
    sensors.append(s)
    s = Sensor('STS-2 gen3')
    s.NRL_keys = ('Streckeisen', 'STS-2', '1500', '3 - installed 04/97 to present')
    sensors.append(s)

    d = DataLogger('Na')
    dataloggers.append(d)
    d = DataLogger('RT-130')
    d.NRL_keys = ('REF TEK', 'RT 130 & 130-SMA', PlaceHolder.GAIN, PlaceHolder.SR)
    d.soh_resps['LOG'] = None
    d.soh_resps['VM'] = 'RT130_VM_RESP'
    dataloggers.append(d)
    d = DataLogger('Q330')
    d.NRL_keys = ('Quanterra', 'Q330SR', PlaceHolder.GAIN, PlaceHolder.SR, 'LINEAR AT ALL SPS')
    d.soh_resps['ACE'] = None
    d.soh_resps['LOG'] = None
    d.soh_resps['OCF'] = None
    d.soh_resps['LCE'] = 'Q330_LCE_RESP'
    d.soh_resps['LCQ'] = 'Q330_LCQ_RESP'
    d.soh_resps['VCO'] = 'Q330_VCO_RESP'
    d.soh_resps['VEA'] = 'Q330_VEA_RESP'
    d.soh_resps['VEC'] = 'Q330_VEC_RESP'
    d.soh_resps['VEP'] = 'Q330_VEP_RESP'
    d.soh_resps['VKI'] = 'Q330_VKI_RESP'
    d.soh_resps['VPB'] = 'Q330_VPB_RESP'
    d.soh_resps['VM'] = 'Q330_VM_RESP'
    dataloggers.append(d)

    empty_response = Response()
    for d in dataloggers.values():
        for chan, resp_str in d.soh_resps.items():
            if not resp_str:
                d.soh_resps[chan] = empty_response
            else:
                filename = d.soh_resps[chan]
                with open(os.path.join(resp_dir, filename), 'r') as f:
                    d.soh_resps[chan] = read_inventory(
                        f, format='RESP')[0][0][0].response
