#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Lloyd Carothers
"""
from collections import defaultdict
import os

from obspy import read as obspy_read
from obspy import Inventory
from obspy import UTCDateTime
from obspy.core.inventory import (Network, Station, Channel,
                                  Site, Equipment)
from obspy.core.inventory.util import Comment
from obspy.io.mseed.core import _is_mseed
from obspy.io.mseed import util

from . import hardware

MODULE = 'Nexus.2023.4.0.0'
MODULE_URI = 'www.passcal.nmt.edu'

class InventoryIm(Inventory):
    '''
    Improved inventory object
    '''
    def __init__(self, *args,  **kwargs):
        Inventory.__init__(
            self,
            *args,
            networks=[],
            source=None,
            module=MODULE,
            module_uri=MODULE_URI,
            **kwargs,
        )

    def set_new(self, bool=True):
        '''
        Clears the _new from all objects.
        Should be called before adding new objects
        '''
        for net in self.networks:
            net._new = bool
            for sta in net.stations:
                sta._new = bool
                for chan in sta.channels:
                    chan._new = bool

    def clear_new(self):
        self.set_new(bool=False)

    def populate_from_streams(self, streams, new=False):
        self.clear_new()
        for trace in (traces for stream in streams for traces in stream):
            net = trace.stats.network
            sta = trace.stats.station
            chan = trace.stats.channel
            loc = trace.stats.location
            start = trace.stats.starttime
            end = trace.stats.endtime
            sr = trace.stats.sampling_rate

            self.add_sncl(net, sta, chan, loc, sr,
                          start, end, new=new)

    def scan_slow(self, dir_name, new=False, log_message=print):
        '''
        Scans dir_name for all mseedfiles and adds to self.
        log_message is a callback to log progress
         i.e. files as processed
        Reads all headers of each file with obspy.read
        '''
        # REMOVE for profiling
        import time
        start = time.time()

        num_files = 0
        streams = []
        for root, dirs, files in os.walk(dir_name):
            for name in files:
                abs_path = os.path.join(root, name)
                log_message('Scanning {}...'.format(abs_path))
                if (os.path.isfile(abs_path) and
                     _is_mseed(abs_path) ):
                    try:
                        st = obspy_read(abs_path, headonly=True)
                        num_files += 1
                        streams.append(st)
                    except:
                        print('Failed to read file {}'.format(abs_path))
        self.populate_from_streams(streams, new=new)

        print(f'Scan took: {time.time() - start}')

        return num_files

    def scan_quick(self, dir_name, new=False, log_message=print):
        '''
        Scans dir_name for all mseedfiles and adds to self.
        log_message is a callback to log progress
         i.e. files as processed
        Reads first and last  header of each file with obspy.read
        '''
        # REMOVE for profiling
        import time
        start = time.time()

        num_files = 0
        for root, dirs, files in os.walk(dir_name):
            for file in files:
                path = os.path.join(root, file)
                log_message('Scanning {}...'.format(path))
                try:
                    if _is_mseed(path):
                        with open(path, 'rb') as fp:
                            rec = util.get_record_information(fp)
                            endtime = util.get_start_and_end_time(fp)
                        rec['endtime'] = endtime[1]
                        self.add_from_record_info(rec, new=new)
                        num_files += 1
                except Exception as e:
                    print(e)
                    continue

        print(f'Scan took: {time.time() - start}')

        return num_files

    def add_from_record_info(self, rec, new=False):
        '''
        For quick mseed scan
        rec is the return of obspy.io.mseed.util.get_record_information
        usually you will want to replace endtime with endtime of
        last record
        '''
        self.add_sncl(rec['network'],
                      rec['station'],
                      rec['channel'],
                      rec['location'],
                      rec['samp_rate'],
                      rec['starttime'],
                      rec['endtime'],
                      new=new,
                      )


    def add_sncl(self, net, sta, chan, loc, sr, start, end, new=False):
        # This is based on obspy.inventory.select behaviour which seems to
        # return copies of objects except for leaves.
        # First find if the sncl exists.
        # if one, then extend
        # if more than one (time split, as a sensor split should not have the
        # same channel in more than one station.
        # then find the closest or station.
        existing = self.select(network=net,
                               station=sta,
                               channel=chan,
                               location=loc,
                               sampling_rate=sr)
        if len(existing.networks) == 1:
            # There are full matching channels, possibly more than 1 station
            # if it was split across time as well as sensor.

            # find the right station
            tmp_net = existing.networks[0]

            tmp_station = self.get_closest_epoch(tmp_net, start, end)
            inv_chan = tmp_station[0]
            inv_sta = self.get_parent(inv_chan)
            inv_net = self.get_parent(inv_sta)

            inv_chan = inv_sta.channels[inv_sta.channels.index(inv_chan)]
            if self.extend_epoch(inv_chan, start, end):
                inv_chan._new = new
            if self.extend_epoch(inv_sta, start, end):
                inv_sta._new = new
            if self.extend_epoch(inv_net, start, end):
                inv_net._new = new
            return

        # Below happens if something new needs to be created

        # Find what exists in current inventory
        #Network
        existing = self.select(network=net)
        # remove for debugging
        assert isinstance(existing, InventoryIm)

        inv_net = self.get_closest_epoch(existing.networks,
                                         start, end)
        if inv_net is None:
            inv_net = self.new_network(code=net)
            inv_net.start_date = start
            inv_net.end_date = end
        else:
            inv_net = self.networks[self.networks.index(inv_net)]
            if self.extend_epoch(inv_net, start, end):
                inv_net._new = new

        # Station
        existing = inv_net.select(station=sta)
        inv_sta = self.get_closest_epoch(existing.stations,
                                         start, end)
        if inv_sta is None:
            inv_sta = self.new_station(inv_net, code=sta)
            inv_sta.start_date = start
            inv_sta.end_date = end
        else:
            inv_sta = inv_net.stations[inv_net.stations.index(inv_sta)]
            if self.extend_epoch(inv_sta, start, end):
                inv_sta._new = new

        # Channel
        existing = inv_sta.select(channel=chan,
                                  location=loc,
                                  sampling_rate=sr)
        # This shouldn't happend a station shouldn't have 2 matching chans,
        # but it could have 0, and return None
        inv_cha = self.get_closest_epoch(existing.channels,
                                         start, end)
        if inv_cha is None:
            inv_cha = self.new_channel(inv_sta, code=chan,loc=loc, sr=sr)
            inv_cha.start_date = start
            inv_cha.end_date = end
        else:
            inv_cha = inv_sta.channels[inv_sta.channels.index(inv_cha)]
            if self.extend_epoch(inv_cha, start, end):
                inv_cha._new = new

        # add Comment object to Channel
        comment = Comment('')
        inv_cha.comments.append(comment)

    def extend_epoch(self, inv, start, end):
        ret = False
        if inv.start_date > start:
            inv.start_date = start
            ret = True
        if inv.end_date < end:
            inv.end_date = end
            ret = True
        return ret

    def get_closest_epoch(self, inv, start, end):
        '''
        typically for finding the correct object to extend
        inv can be a networks, stations or channels list
        returns the closest by time or None with the same
        short circuts and returns object if timespans overlap
        type as inv
        '''
        if len(inv) < 1:
            return None
        elif len(inv) == 1:
            #return the 1 match
            return inv[0]
        elif len(inv) > 1:
            # return the closest by time
            # minimun of end-start and start-end
            ret = inv[0]
            if self.overlap(ret, start, end):
                return ret
            dist = min(abs(ret.start_date - end),
                       abs(ret.end_date - start))
            for obj in inv[1:]:
                if self.overlap(obj, start, end):
                    return obj
                new_dist = min(abs(obj.start_date - end),
                               abs(obj.end_date - start))
                if new_dist < dist:
                    ret = obj
                    dist = new_dist
            return ret

    def overlap(self, inv, start, end):
        '''
        inv must have a start_date end_date
        returns True if start-end overlap
        '''
        # other is before with overlap
        if inv.start_date <= end and inv.end_date >= start:
            return True
        return False


    def populate_from_streams_old(self, streams, new=False):
        self.clear_new()
        for stream in streams:
            for trace in stream:

                start = trace.stats.starttime
                end = trace.stats.endtime

                # Network
                net = trace.stats.network
                if net not in (n.code for n in self.networks):
                    inv_net = self.new_network(code=net)
                    inv_net.start_date = start
                    inv_net.end_date = end
                else:
                    inv_net = [n for n in self.networks if n.code == net][0]
                    if inv_net.start_date > start:
                        inv_net.start_date = start
                    if inv_net.end_date < end:
                        inv_net.end_date = end
                inv_net._new = new

                # Station
                sta = trace.stats.station
                if sta not in (s.code for s in inv_net):
                    inv_sta = self.new_station(inv_net, code=sta)
                    inv_sta.start_date = start
                    inv_sta.end_date = end
                else:
                    inv_sta = [s for s in inv_net if s.code == sta][0]
                    if inv_sta.start_date > start:
                        inv_sta.start_date = start
                    if inv_sta.end_date < end:
                        inv_sta.end_date = end
                inv_sta._new = new

                # test
                # this kinda stuff should be broken out
                # Maybe StationIm class?
                #inv_sta.depth = 0

                # Channel
                chan = trace.stats.channel
                loc = trace.stats.location
                sr = trace.stats.sampling_rate
                # Does sncl + sr exist
                # TODO: may need to select on net see Alissa test data
                existing = inv_sta.select(channel = chan,
                                       location = loc,
                                       sampling_rate=sr)
                if len(existing.channels) == 0:
                    inv_chan = self.new_channel(inv_sta, code=chan,
                                                loc=loc, sr=sr)
                    inv_chan.start_date = start
                    inv_chan.end_date = end
                elif len(existing.channels) == 1:
                    inv_chan = existing.channels[0]
                    if inv_chan.start_date > start:
                        inv_chan.start_date = start
                    if inv_chan.end_date < end:
                        inv_chan.end_date = end
                else:
                    # This may not be right, but should for streams
                    raise KeyError('More than 1 SNCL w/SR')
                # Cascade up times
                inv_chan._new = new

    def calculate_responses_old(self):
        '''
        obsolete use get_responses
        Calculates and attaches responses to channels that don't have a
        response and have sensor and datalogger type
        '''
        nrl = NRL(root=NRL_ROOT)
        for net in self.networks:
            for sta in net.stations:
                for chan in sta.channels:
                    if (chan.response is None
                            and chan.sample_rate >= 1
                            and hasattr(chan.sensor, 'type')
                            and chan.sensor.type in SENSORS.names()
                            and hasattr(chan.data_logger, 'type')
                            and chan.data_logger.type in DLS.names()
                            and hasattr(chan.data_logger, 'gain')
                            ):
                        sensor_keys = SENSORS.keys_from_name(chan.sensor.type)
                        gain = '{:g}'.format(chan.data_logger.gain)
                        sr = '{:g}'.format(chan.sample_rate)
                        dl = DLS.get(chan.data_logger.type)
                        dl_keys = dl.get_nrl_keys(gain, sr)
                        #dl_keys = deepcopy(dl_keys)
                        #sensor_keys = deepcopy(sensor_keys)
                        try:
                            response = nrl.get_response(dl_keys, sensor_keys)
                            chan.response = response
                        except Exception as e:
                            print(e)

    def calculate_responses(self):
        '''
        Calculates NRL responses for seismic channels.
        Fetches SOH channel responses from config file
        '''
        DLS = hardware.dataloggers
        SENSORS = hardware.sensors
        for (net,sta,chan) in ((n,s,c) for
                               n in self.networks for
                               s in n.stations for
                               c in s.channels):
            print(net.code, sta.code, chan.code, end=' ')
            # Leave responses if they exist
            if chan.response is not None:
                print('Response exists, skipping.')
                continue
            # Check datalogger needed for seismo and SOH
            if not (hasattr(chan.data_logger, 'type')
                    and chan.data_logger.type in DLS.names()):
                print ('Datalogger not defined, skipping.')
                continue
            dl = DLS.get(chan.data_logger.type)
            # Geophysical seismic channel use sensor and datalogger
            if hardware.chan_name_is_geophysical(chan.code):
                if not (hasattr(chan.sensor, 'type') and
                        chan.sensor.type in SENSORS.names()):
                    print ('Sensor not defined, skipping.')
                    continue
                sensor_keys = SENSORS.keys_from_name(chan.sensor.type)
                if dl.requires_gain():
                    if not hasattr(chan.data_logger, 'gain'):
                        print('Datalogger requires gain, not provided, skipping')
                        continue
                    else:
                        gain = '{:g}'.format(chan.data_logger.gain)
                else:
                    gain = None
                sr = '{:g}'.format(chan.sample_rate)
                dl_keys = dl.get_nrl_keys(gain, sr)
                try:
                    response = hardware.get_nrl_response(dl_keys,
                                                         sensor_keys)
                    chan.response = response
                    print('Response computed.')
                except Exception as e:
                    print('Failed to compile response from NRL with keys:')
                    print('\t', dl_keys)
                    print('\t', sensor_keys)
                    print('\t', e)
            # SOH channel
            else:
                print('SOH', end=' ')
                try:
                    chan.response = dl.get_soh_resps(chan.code)
                except Exception as e:
                    print('SOH channel not found in {}'.format(dl.soh_resps))
                    continue
                print('Response from hardware used.')

    def middle_epoch(self, obj):
        assert hasattr(obj,'start_date') and hasattr(obj, 'end_date')
        duration = obj.end_date - obj.start_date
        return obj.start_date + duration / 2

    def split_station_by_epoch(self, station, time=None):
        '''
        Creates a copy of station.
        The first ends and time and the copy starts at time
        '''
        assert isinstance(station, Station)
        if time is None:
            time = self.middle_epoch(station)

        sta1 = station.select(endtime=time).copy()
        sta2 = station.select(starttime=time).copy()

        # Set sta1's endtime active at split
        sta1.end_date = time
        active = sta1.select(time=time)
        for chan in active.channels:
            chan.end_date = time

        # Set sta2's starttime
        sta2.start_date = time
        active = sta2.select(time=time)
        for chan in active.channels:
            chan.start_date = time

        for net in self.networks:
            if station in net.stations:
                network = net
                break

        # Remove original
        place = network.stations.index(station)
        network.stations.remove(station)

        # Display as new
        self.clear_new()
        sta1._new = True
        sta2._new = True

        # add the 2 new stations
        network.stations.insert(place, sta2)
        network.stations.insert(place, sta1)

    def split_station_by_channels(self, station, channels):
        '''
        Creates a copy of station.
        channels are moved to the copy.
        '''
        assert isinstance(station, Station)
        station2 = station.copy()
        # Loop over original channels
        for orig_chan in list(station.channels):
            if orig_chan in channels:
                station.channels.remove(orig_chan)
            else:
                station2.channels.remove(orig_chan)

        self.clear_new()
        station._new = True
        station2._new = True

        network = self.get_parent(station)
        place = network.stations.index(station) + 1
        network.stations.insert(place, station2)

    def delete(self, item):
        '''
        Removes a net, sta, or channel
        returns parent so can be highlighted
        '''
        parent = self.get_parent(item)
        if isinstance(item, Network):
            parent.networks.remove(item)
            return parent
        elif isinstance(item, Station):
            parent.stations.remove(item)
            return parent
        elif isinstance(item, Channel):
            parent.channels.remove(item)
            return parent
        return False

    def add(self, item):
        ''' adds an appropriate child to item.
            returns the item so it can be highlighted
        '''
        new = None
        self.clear_new()
        if isinstance(item, self.__class__):
            new = Network()
            item.networks.append(new)
        elif isinstance(item, Network):
            new = StationIm('XXXXX', 0.0, 0.0, 0, site=Site(''),
                            creation_date=UTCDateTime.now())
            item.stations.append(new)
            new_chan = Channel('XXX', '', 0.0, 0.0, 0, 0)
            new_chan._new = True
            new.channels.append(new_chan)
        elif isinstance(item, Station):
            new = Channel('XXX', '', 0.0, 0.0, 0, 0)
            item.channels.append(new)
        if new:
            new._new = True
            return new
        return False

    def get_parent(self, item):
        '''
        Returns self (inventory) for network,
                net for station,
                sta for channel.
        '''
        for net in self.networks:
            if item is net:
                return self
            for sta in net:
                if item is sta:
                    return net
                for chan in sta.channels:
                    if item is chan:
                        return sta
        return None


    def print_contents(self):
        for net in self.networks:
            print(net)
            print()
            for sta in net:
                print('\t', sta)
                print()
                for chan in sta:
                    print('\t\t', chan)
                    print()
                print()
            print()

    def print_sum(self):
        '''
        print sync like summary
        '''
        def utc_to_str(utc):
            line = ''
            line += f'{utc.year:>4d} '
            line += f'{utc.julday:>03d} '
            line += f'{utc.hour:>02d}:'
            line += f'{utc.minute:>02d} '
            #line += f'{utc.second:>02d}'
            #line += f'.{utc.microsecond:>06d}'
            return line
        for net,sta, chan in ((net, sta, chans)
                               for net in self.networks
                               for sta in net.stations
                               for chans in sta.channels):
            line = ''
            line += f'{net.code:<2s} '
            line += f'{sta.code:<5s} '
            line += f'{chan.location_code:>2s} '
            line += f'{chan.code:<3s}  '
            line += f'{utc_to_str(chan.start_date):s}-- '
            line += f'{utc_to_str(chan.end_date):s} '
            line += f'{chan.sample_rate:>4.0f} '

            print(line)

    # should implement this and use in populate loop
    def new_network(self, code='XX'):
        start = UTCDateTime(year=UTCDateTime().now().year, julday=1 )
        end = UTCDateTime(year=start.year + 1, julday=1)

        try:
            inv_net = Network(code=code)
            inv_net.start_date = start
            inv_net.end_date = end
            self.networks.append(inv_net)
        except ValueError as e:
            print('Could not create network {}'.format(e))
            return False
        inv_net._new = True
        return inv_net

    def new_station(self, net, code='XXXXX'):
        inv_sta = Station(
            code, latitude=0, longitude=0, elevation=0,
            site=Site(''),
            creation_date=UTCDateTime.now())
        inv_sta.start_date = net.start_date
        inv_sta.end_date = net.end_date
        net.stations.append(inv_sta)
        inv_sta._new = True
        return inv_sta


    def new_channel(self, station, code='XXX', loc='', sr=0):
        az = 'az'
        dip = 'dip'
        orient = defaultdict(lambda: {az: 0.0, dip: 0.0},
                             {'Z': {az: 0.0, dip: -90.0},
                              'N': {az: 0.0, dip: 0.0},
                              '1': {az: 0.0, dip: 0.0},
                              'E': {az: 90.0, dip: 0.0},
                              '2': {az: 90.0, dip: 0.0},
                              })
        # Mass positions don't have this az and dip, NOR LOG
        if code[:2] == 'VM' or code == 'LOG':
            azimuth = None
            dip_chan = None
        else:
            azimuth = orient[code[2]][az]
            dip_chan = orient[code[2]][dip]
        inv_chan = Channel(
            code, loc, latitude=0, longitude=0, elevation=0,
            depth=0, sample_rate=sr, azimuth=azimuth, dip=dip_chan)
        inv_chan.start_date = station.start_date
        inv_chan.end_date = station.end_date
        try:
            inv_chan.sensor = station.channels[0].sensor
        except:
            inv_chan.sensor = Equipment()
        try:
            inv_chan.data_logger = station.channels[0].data_logger
        except:
            inv_chan.data_logger = Equipment()
        # add location from parent station
        inv_chan.latitude = station.latitude
        inv_chan.longitude = station.longitude
        inv_chan.elevation = station.elevation
        try:
            inv_chan.depth = station.channels[0].depth
        except:
            inv_chan.depth = 0
        station.channels.append(inv_chan)
        inv_chan._new = True
        return inv_chan


# Utitlity functions. Live elsewhere?
def utc_to_str(date):
    try:
        return '{}.{:03d}.{:02d}:{:02d}.{:>02d}.{:>06d}'.format(date.year,
                                                                date.julday,
                                                                date.hour,
                                                                date.minute,
                                                                date.second,
                                                                date.microsecond)
    except:
        try:
            return str(date)
        except:
            pass
    return ''

def utc_from_str(value):
    year, jday, *rest = value.split('.')
    year, jday = int(year), int(jday)
    if rest:
        hour, *minute = rest[0].split(':')
        second = int(rest[1])
        microsecond = int(rest[2])
    else:
        hour, minute, second, microsecond = 0, 0, 00, 000000
    if hour:
        hour = int(hour)
    else:
        hour = 0
    if minute and minute[0]:
        minute = int(minute[0])
    else:
        minute = 0
    return UTCDateTime(year=year, julday=jday, hour=hour, minute=minute, second=second, microsecond=microsecond)

def scan_ms(dir_name, status_message=print):
    # Remove timing test
    import time
    start = time.time()

    mseed_files = []
    streams = []
    for root, dirs, files in os.walk(dir_name):
        for name in files:
            abs_path = os.path.join(root, name)
            status_message('Scanning {}...'.format(abs_path))
            if ( os.path.isfile(abs_path) and
                 _is_mseed(abs_path) ):
                mseed_files.append(abs_path)
                try:
                    st = obspy_read(abs_path, headonly=True)
                except:
                    print('Failed to read file {}'.format(abs_path))
                streams.append(st)
    print(f'Scan took: {time.time() - start}')
    return streams, mseed_files

def quick_scan_ms(dir_name, status_message=print):
    import time
    start = time.time()

    print(f'Scan took: {time.time() - start}')
