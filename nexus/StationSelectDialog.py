#!/usr/bin/env python
# -*- coding: utf-8 -*-*#
"""
Dialog for selecting stations to copy
Datalogger Type, Sensor Type and Gain
"""

import os

from PySide6.QtGui import QFontMetrics, Qt
from PySide6.QtWidgets import QTableWidgetItem, QAbstractItemView, QHeaderView
from PySide6.QtUiTools import loadUiType

from .obspyImproved import utc_to_str

def load_ui(filename):
    """
    Helper function
    Load a ui file relative to this source file
    """
    path = os.path.join(os.path.dirname(__file__), filename)
    try:
            ret = loadUiType(path)
    except Exception as e:
        print(e)
        raise e
    return ret

class StationSelectDialog(*load_ui('StationSelectDialog.ui')):
    def __init__(self, stations, action, values, parent=None):
        super().__init__(parent)
        self.stations = stations
        self.sta_map = {}
        self.action = action
        self.values = values
        self.setupUi()

    def accept(self):
        rows = set(item.row() for item in self.tableWidget.selectedItems())
        self.selected_stations = []
        for row in rows:
            self.selected_stations.append(self.sta_map[row])
        super().accept()

    def sort_table(self, column):
        """
        Sort Station Select Dialog table based on column
        header selected by user.
        """
        row_map = {0: 'code', 1: 'start_date', 2: 'end_date'}
        order = self.tableWidget.horizontalHeader().sortIndicatorOrder()
        descending = False if order == Qt.AscendingOrder else True
        self.sta_map = {ind: v for ind, (_, v) in enumerate(sorted(self.sta_map.items(),
                                                            key=lambda item: getattr(item[1], row_map[column]),
                                                            reverse=descending))}

    def setupUi(self):
        super().setupUi(self)
        self.setWindowTitle('Select Stations')
        if self.action == 'type & gain':
            self.uiGainCB.setChecked(True)
            self.uiGainCB.setText(f"Copy Datalogger Gain: {self.values[0]}")
            self.uiTypeCB.setChecked(True)
            self.uiTypeCB.setText(f"Copy Datalogger Type: {self.values[1]}")
            self.uiSensorLabel.setParent(None)
        else:
            self.uiSensorLabel.setText(f"Copy Sensor Type: {self.values[0]}")
            self.uiTypeCB.setParent(None)
            self.uiGainCB.setParent(None)
        self.tableWidget.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tableWidget.setRowCount(len(self.stations))
        self.tableWidget.horizontalHeader().sectionClicked.connect(self.sort_table)
        # Add stations to table
        for row, sta in enumerate(self.stations):
            for col, value in enumerate(('{:<3s}'.format(sta.code),
                                         utc_to_str(sta.start_date),
                                         utc_to_str(sta.end_date))):
                item = QTableWidgetItem()
                item.setText(value)
                # shorten start/end time
                if col == 1 or col == 2:
                    # full time as tooltip
                    item.setToolTip(value)
                    # rounded time in table
                    value = QFontMetrics(self.tableWidget.font()).elidedText(value,
                                                                             Qt.ElideRight,
                                                                             100)
                    item.setText(value)
                self.tableWidget.setItem(row, col, item)
            self.sta_map[row] = sta
        self.tableWidget.resizeColumnsToContents()
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
